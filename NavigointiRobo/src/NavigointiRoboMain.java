
import lejos.hardware.motor.Motor;
import lejos.robotics.geometry.*;
import lejos.robotics.geometry.Rectangle;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.pathfinding.PathFinder;
import lejos.robotics.pathfinding.ShortestPathFinder;
import lejos.robotics.mapping.LineMap;

public class NavigointiRoboMain {

	public static void Main(String[] args) {
		//luodaan huone ja janat
		Rectangle huone = new Rectangle(0,0,100,140);
		Line[] janat = new Line[10];
		// huoneen sivut
		janat[0] = new Line (0,0,140,0);
		janat[1] = new Line (140,0,140,100);
		janat[2] = new Line (0,100,150,100);
		janat[3] = new Line (0,0,0,100);
		// ylävasen väliseinä
		janat[4] = new Line (0,60,40,60);
		// keski väliseinä
		janat[5] = new Line (80,0,80,40);
		// yläoikea huonekalu
		janat[6] = new Line (120,70,1330,80);
		janat[7] = new Line (130,80,120,90);
		janat[8] = new Line (120,90,110,80);
		janat[9] = new Line (110,80,120,70);
		LineMap kartta = new LineMap(janat, huone);

		//luodaan navigaattori

        DifferentialPilot pilot = new DifferentialPilot(5.0f, 11.0f, Motor.B, Motor.C, true);
        Navigator navigator = new Navigator(pilot);

        //aloituspiste
        Pose alkusijainti = new Pose(0, 30, 0);

        //välipiste
        Waypoint kohde = new Waypoint(20.0f, 80.0f);

        //Etsi tie
        PathFinder polunetsijä = new ShortestPathFinder(kartta);
        ((ShortestPathFinder) polunetsijä).lengthenLines(10);
        Path polku = null;
		try {
			polku = polunetsijä.findRoute(alkusijainti, kohde);
		} catch (DestinationUnreachableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        //aloita ajaminen
        navigator.setPath(polku);
        navigator.followPath();
        navigator.waitForStop();


	}
}
