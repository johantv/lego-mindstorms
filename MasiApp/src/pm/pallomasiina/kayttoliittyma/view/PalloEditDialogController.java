package pm.pallomasiina.kayttoliittyma.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pm.pallomasiina.kayttoliittyma.model.Pallo;

/**
*Editointi-ikkunan controlleri. Sisältää edit ikkunan metodeja.
*
* @author Ryhmä 8
* @version versionumero
*/
public class PalloEditDialogController {

    @FXML
    private TextField variField;
    @FXML
    private TextField maaraField;

    private Stage dialogStage;
    private Pallo pallo;
    private boolean okClicked = false;

    /**
     * Alustaminen. Tapahtuu automaattisesti kun fxml tiedosto ladataan.
     *
     */
    @FXML
    private void initialize() {
    }

    /**
     *Asettaa stagen editointi-ikkunalle.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;

        // Set the dialog icon.
        this.dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
    }

    /**
     * Setteri joka tuo tiedot editoitavalle pallo-oliolle.
     *
     * @param pallo
     */
    public void setPerson(Pallo pallo) {
        this.pallo = pallo;

        variField.setText(pallo.getVari());
        maaraField.setText(Integer.toString(pallo.getMaara()));

    }

    /**
     * Metodi joka palauttaa true, jos painettiin ok.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Ok-painikkeen metodi.
     * kutsutaan, jos kayttaja painoi ok. Asettaa tiedot uudelle pallolle.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            pallo.setVari(variField.getText());

            pallo.setMaara(Integer.parseInt(maaraField.getText()));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Peruutta-painikkeen metodi.
     * kutsutaan, jos kayttaja painoi Peruuta.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Tarkistaa sisaltaako tekstikenttiin syotetty data virheellisia merkkeja.
     *
     * @return Palauttaa true jos syotetty data on ok.
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (variField.getText() == null || variField.getText().length() == 0) {
            errorMessage += "Vari ei kelpaa!\n";
        }
        if (maaraField.getText() == null || maaraField.getText().length() == 0) {
            errorMessage += "Maara ei ole kelvollinen!\n";
        }else{
            try {
                Integer.parseInt(maaraField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Syotit maaraan virheellisia merkkeja! (Kayta kokonaislukuja)!\n";
            }
        }



        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Error.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Virheellinen syote");
            alert.setHeaderText("Korjaa virheelliset kentat!");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}