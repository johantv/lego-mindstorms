package pm.pallomasiina.kayttoliittyma.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Apuluokka pallolistan kasaamiseen. Käytetään XML tiedoston tallennuksessa.
 * 
 * @author
 */
@XmlRootElement(name = "persons")
public class PalloListWrapper {

    private List<Pallo> persons;

    @XmlElement(name = "person")
    public List<Pallo> getPersons() {
        return persons;
    }

    public void setPersons(List<Pallo> pallot) {
        this.persons = pallot;
    }
}