package pack;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;

import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;



public class Värianturit extends LegoRobotti{

	//(Muuttujat
	public static int red, green, blue;

	//Värianturi
	EV3ColorSensor colorSensor;
	SampleProvider colorProvider;
	float[] colorSample;

	Port s3 = LocalEV3.get().getPort("S3");
	colorSensor = new EV3ColorSensor(s3);
	colorProvider = colorSensor.getColorIDMode();
	colorSample = new float[colorProvider.sampleSize()];


	public int tunnistaVäri() {
		
	}


	public Värianturit() {
		//tunnistetaan antureilla väri ja tallennetaan se ensimmäiseen väriin
		boolean loop=true;
		while (loop==true) {
			int currentDetectedColor = colorSensor.getColorID();
			red = Math.round(colorSample[0])*765;
			green = Math.round(colorSample[1])*765;
			blue = Math.round(colorSample[2])*765;
		}
		TeeVari1();
	}


	public void TeeVari1() {
		väri1[0] = red;
		väri1[1] = green;
		väri1[2] = blue;
	}

	public void TeeVari2() {
		väri2[0] = red;
		väri2[1] = green;
		väri2[2] = blue;
	}

	public void laskeVari() {
		//etäisyys
		väriErotus[0] = väri1[0] - Värianturit.red;
		väriErotus[1] = väri1[1] - Värianturit.green;
		väriErotus[2] = väri1[2] - Värianturit.blue;


		}}
	//Tallenna havaittu väri

	//Tarvittavat muuttujat & listat

	//int[] väri1;
	int[] väri2;
	int[] väri1 = {};
	int[] väriErotus;
	int[] väriNeliö;

}






	//pythagora eli neliö ja ota neliö juuri






