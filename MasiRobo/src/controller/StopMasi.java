package controller;

import lejos.hardware.Button;
import lejos.robotics.subsumption.Behavior;
import model.MoottoritAB;
import model.SokettiRobotti;

public class StopMasi implements Behavior {
	private volatile boolean suppressed = false;
	SokettiRobotti robo=new SokettiRobotti();
    private MoottoritAB flipperit;

    // Konstruktori
    /**
     * Konstruktori.
     * @param flipperit Moottorit joita StopMasi hyödyntää.
     */
    public StopMasi(MoottoritAB flipperit) {
    	this.flipperit=flipperit;
    }

    /**
     * Ehto käyttäytymisen käynnistämiseen.
     */
	public boolean takeControl() {
		if (Button.LEFT.isDown()) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Lopeta kayttäytyminen.
	 */
	public void suppress() {
		suppressed = true;
	}

	/**
	 * Suoritettava käyttäytyminen.
	 * Pysähdy kunnes konsolin Enter-nappia painetaan-->palauta moottorit lähtö asentoon.
	 */
	public void action(){
		suppressed = false;
		// Kun nappia painetaan, robotti resettaa flippereiden sijainnin
        Button.ENTER.waitForPress() ;
        flipperit.resetMoottorit();
	}
}