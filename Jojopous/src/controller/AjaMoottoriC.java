package controller;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import model.MoottoriC;
import model.Toimintojentila;
import model.Varianturi;

public class AjaMoottoriC implements Behavior {

    private Varianturi vari;
    private MoottoriC nostin;
    private volatile boolean suppressed = false;
    private boolean kontrolli = true;
    private boolean tehty;
    private Toimintojentila tila;

    public AjaMoottoriC (MoottoriC nostin, Toimintojentila tila) {
    	this.tila = tila;
    	this.nostin = nostin;
    	tehty = false;
    }

    public boolean takeControl() {
		return tila.getActionB();
	}

	public void action() {
		suppressed = false;

		while(!suppressed) {
			nostin.suoraan();
			suppressed =true;
		}

		tila.setActionA(true);
		tila.setActionB(false);
	}

	public void suppress() {
		suppressed = true;
	}

    public void kaannyP() {
    	nostin.suoraan();
        tehty = true;
    }

    public boolean onkoTehty() {
    	if(tehty == true) {
    		tehty = false;
    		return false;
    	}
    	else {
    		return true;
    	}
    }
}