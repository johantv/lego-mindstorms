package model;

import lejos.hardware.Button;
import lejos.robotics.subsumption.Behavior;

public class Nappikomennot implements Behavior {
	private volatile boolean suppressed = false;

	SokettiRobotti robo = new SokettiRobotti();

	//Vikana päivänä lisätty
    private MoottoritAB flipperit;

    // Konstruktori
    public Nappikomennot(MoottoritAB flipperit) {
    		this.flipperit=flipperit;
    }

	public boolean takeControl(){
		if(Button.ENTER.isDown() ) {
			return true;
		}
		else {
			return false;
		}
	}

	public void suppress() {
		suppressed = true;
	}

	public void action() {
		suppressed = false;
        flipperit.resetMoottorit();

		System.exit(0);
		while(!suppressed)Thread.yield();
	}
}