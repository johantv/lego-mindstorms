package main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import controller.AjaMoottoritAB;
import controller.StopMasi;
import lejos.robotics.subsumption.*;
import lejos.utility.Delay;
import model.MoottoritAB;
import model.Varianturi;
import controller.AjaMoottoriC;
import model.MoottoriC;
import model.Nappikomennot;
import model.SokettiRobotti;
import model.Toimintojentila;

public class main {

	public static void main(String[] args) {
		Varianturi vari = new Varianturi();
		SokettiRobotti s = new SokettiRobotti();
		MoottoritAB moottoritAB = new MoottoritAB();
		MoottoriC moottoriC = new MoottoriC();
		Toimintojentila tila = new Toimintojentila();

		Behavior valmisteleFlipperit = new AjaMoottoritAB(moottoritAB,vari,tila,s);
		Behavior laukaisePallo = new AjaMoottoriC(moottoriC, tila);
		Behavior pysäytäKoodi = new Nappikomennot(moottoritAB);
		Behavior stoppaaMasi = new StopMasi(moottoritAB);

		s.kaynnistaYhteys();

		Behavior [] bArr = {laukaisePallo,pysäytäKoodi,stoppaaMasi,valmisteleFlipperit};

		Arbitrator arby = new Arbitrator(bArr);
		arby.go();
	}
}