package model;

import lejos.hardware.Button;
import lejos.robotics.subsumption.Behavior;

/**
* Käyttäytyminen joka toimii hätäseis-ominaisuutena
*
* @author Ryhmä 8
* @version versionumero
*/
public class Nappikomennot implements Behavior {
	private volatile boolean suppressed = false;

	SokettiRobotti robo = new SokettiRobotti();


    private MoottoritAB flipperit;


    /**
     * Konstruktori
     * @param flipperit
     */
    public Nappikomennot(MoottoritAB flipperit) {
    		this.flipperit=flipperit;
    }
    /**
     * Ehto käyttäytymisen käynnistämiseen.
     */
	public boolean takeControl(){
		if(Button.ENTER.isDown() ) {
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Lopeta kayttäytyminen.
	 */
	public void suppress() {
		suppressed = true;
	}
	/**
	 * Suoritettava käyttäytyminen.
	 * Palauta moottorit lähtöasentoon-->Sammuta ohjelma.
	 */
	public void action() {
		suppressed = false;
        flipperit.resetMoottorit();

		System.exit(0);
		while(!suppressed)Thread.yield();
	}
}