package model;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.motor.BasicMotor;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

// Moottorit siivekkeille (A ja B)
public class MoottoritAB  {
    RegulatedMotor moottoriA = new EV3LargeRegulatedMotor(MotorPort.A); // Alemmat siivekkeet toimivat yhdessä
    RegulatedMotor moottoriB = new EV3LargeRegulatedMotor(MotorPort.B); // Ylempi siiveke

    //moottorien kääntöasteet
	int Aoikealle = 35;
	int Avasemmalle = -35;
	int Boikealle = -35;
	int Bvasemmalle = 35;

	//Moottorien asennon tarkistusarvot
	int resetArvoA = 0;
	int resetArvoB = 0;

    private boolean kesken = false;

    //Pallon ohjaaminen moottoreilla oikeaan lokeroon
    public void ohjaaPunainen() {
        // Moottorin A nopeuden määritys
        moottoriA.setSpeed(500);

        // Punaisen pallon reitti
        moottoriB.rotate(Bvasemmalle);
        moottoriA.rotate(Avasemmalle);
        odotaMoottorit();
        Delay.msDelay(200);

        // Päivitetään siivekkeiden sijainti
        resetArvoA+=Avasemmalle;
        resetArvoB+=Bvasemmalle;
    }
    
    

    public void ohjaaVihreä() {
        moottoriA.setSpeed(500);

        // Vihreän pallon reitti
        moottoriB.rotate(Bvasemmalle);
        moottoriA.rotate(Aoikealle);
        odotaMoottorit();
        Delay.msDelay(200);

        // Päivitetään flipperien sijainti
        resetArvoB+=Bvasemmalle;
        resetArvoA+=Aoikealle;
    }

    public void ohjaaSininen() {
        moottoriA.setSpeed(500);

        // Sinisen pallon reitti
        moottoriB.rotate(Boikealle);
        moottoriA.rotate(Avasemmalle);
        odotaMoottorit();
        Delay.msDelay(200);

        //päivitetään flipperien sijainti
        resetArvoB+=Boikealle;
        resetArvoA+=Avasemmalle;

    }

    public void ohjaaMuu() {
        moottoriA.setSpeed(500);

        // Muun väristen pallojen reitti
        moottoriB.rotate(Boikealle);
        moottoriA.rotate(Aoikealle);
        odotaMoottorit();
        Delay.msDelay(200);

        // Päivitetään flipperien sijainti
        resetArvoB+=Boikealle;
        resetArvoA+=Aoikealle;
    }

    // Moottorit toimivat samanaikaisesti (synkronointia ei tarvita)
    public void odotaMoottorit() {
		moottoriB.waitComplete();
		moottoriA.waitComplete();
    }

    //Käännä moottoreita haluttu määrä
    public void käännäA(int astetta) {
		moottoriA.rotate(astetta);
		
    }
    public void käännäB(int astetta) {
    		moottoriB.rotate(astetta);
    }
    
    // Moottorien resettaus
    public void resetMoottorit() {
    	// Tarkistetaan onko moottorit kääntyneet lähtöasennosta --> Jos on, käännetään takaisin
    	try {
    		if (resetArvoA!=0 || resetArvoB !=0) {
    			käännäA(resetArvoA*-1);
    			käännäB(resetArvoB*-1);
    	        odotaMoottorit();
    		}
    	}

    	catch(Exception e) {
    		System.out.println("Moottoreita resetatessa tapahtui virhe");
    	}

    	finally {
    		//Resettaa tarkistusarvon
    		resetArvoA=0;
    		resetArvoB=0;
    	}
    }
    		//Returnaa moottorien asento
    public int getMootorinAsentoA() {
    		return resetArvoA;
    }

    public int getMoottorinAsentoB() {
    		return resetArvoB;
    }
    		//käytetäänkö missään?
    public boolean onkoKesken() {
    	if(kesken == true) {
    		kesken = false;
    		return true;
    	}
    	else {
    		return false;
    	}
    }
}