package model;

public class Toimintojentila {
	private static boolean actionA = true;
	private static boolean actionB = true;
	
	public static void setActionA(boolean actionA) {
		Toimintojentila.actionA = actionA;
	}
	public static void setActionB(boolean actionB) {
		Toimintojentila.actionB = actionB;
	}
	public static boolean getActionA() {
		return actionA;
	}
	public static boolean getActionB() {
		return actionB;
	}
}
