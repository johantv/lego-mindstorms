package controller;

import lejos.hardware.Button;
import lejos.robotics.subsumption.Behavior;
import model.MoottoritAB;
import model.SokettiRobotti;

public class StopMasi implements Behavior {
	private volatile boolean suppressed = false;
	SokettiRobotti robo=new SokettiRobotti();
    private MoottoritAB flipperit;

    // Konstruktori
    public StopMasi(MoottoritAB flipperit) {
    	this.flipperit=flipperit;
    }

	public boolean takeControl() {
		if (Button.LEFT.isDown()) {
			return true;
		}
		else {
			return false;
		}
	}

	public void suppress() {
		suppressed = true;
	}

	public void action(){
		suppressed = false;
		// Kun nappia painetaan, robotti resettaa flippereiden sijainnin
        Button.ENTER.waitForPress() ;
        flipperit.resetMoottorit();
	}
}