package controller;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import model.MoottoriC;
import model.Toimintojentila;
import model.Varianturi;
/**
* Moottorin C:n(nostimen) käyttäytyminen. Tämä käyttäytyminen kontrolloi nostimen toimintaa.
*
*
* @author Ryhmä 8
* @version versionumero
*/
public class AjaMoottoriC implements Behavior {

    private Varianturi vari;
    private MoottoriC nostin;
    private volatile boolean suppressed = false;
    private boolean kontrolli = true;
    private Toimintojentila tila;


//---------------------------------------------------------------------
    /**
     * Konstruktori AjaMottoriC:lle.
     * @param nostin AjaMoottoriC:n ohjaama moottori.
     * @param tila Parametri joka kertoo onko nostina pyoraytetty.
     */
    public AjaMoottoriC (MoottoriC nostin, Toimintojentila tila) {
    	this.tila = tila;
    	this.nostin = nostin;

    }
    /**
     * Ehto käyttäytymisen käynnistämiseen.
     */
    public boolean takeControl() {
		return tila.getActionB();
	}
    /**
     * Suoritettava käyttäytyminen.
     * Pyöräytä nostinta.
     */
	public void action() {
		suppressed = false;

		while(!suppressed) {
			nostin.suoraan();
			suppressed =true;
		}

		tila.setActionA(true);
		tila.setActionB(false);
	}
	/**
	 * Lopeta käyttäytyminen
	 */
	public void suppress() {
		suppressed = true;
	}




    }
