package model;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.motor.BasicMotor;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

// Ylin moottori (C), joka työntää kielekettä
public class MoottoriC  {
    RegulatedMotor moottoriC = new EV3MediumRegulatedMotor(MotorPort.C);
    private boolean kesken = false;

    public void suoraan() {
        moottoriC.setSpeed(500); // Moottorin nopeus
        moottoriC.rotate(540); // Moottorissa kiinni oleva ratas pyörii yhden kierroksen
        Delay.msDelay(1000); // Viive pallojen eteenpäintyöntämiselle
    }

    public void seis() {
    	moottoriC.stop(); // Pysäyttää moottorin
    }

    public boolean onkoKesken() {
    	if (kesken == true) {
    		kesken = false;
    		return true;
    	}
    	else {
    		return false;
    	}
    }
}