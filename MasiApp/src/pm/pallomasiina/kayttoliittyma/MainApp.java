package pm.pallomasiina.kayttoliittyma;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pm.pallomasiina.kayttoliittyma.model.ConnectThread;
import pm.pallomasiina.kayttoliittyma.model.Pallo;
import pm.pallomasiina.kayttoliittyma.model.PalloListWrapper;
import pm.pallomasiina.kayttoliittyma.view.PalloOverviewController;
import pm.pallomasiina.kayttoliittyma.view.PalloEditDialogController;
import pm.pallomasiina.kayttoliittyma.view.RootLayoutController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.omg.CORBA.portable.InputStream;
/**
*Paaohjelma joka kaynnistaa "palvelut" ,antaa oikeudet kontrollereille lataa fxml tiedostot.
*
*
* @author Ryhmä 8
* @version versionumero
*/
public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

//Kuvia
	public  static Pallo punainen=new Pallo("Punainen Masi",0);
	public static Pallo vihrea=new Pallo("Vihrea Masi",0);
	public static Pallo sininen=new Pallo("Sininen Masi",0);
	public static Pallo random=new Pallo("Random Masi",0);
	public static ConnectThread ct = new ConnectThread();

    /**
     * Observable lista pallo olioille.
     */
    private ObservableList<Pallo> palloData = FXCollections.observableArrayList();

    /**
     * Konstruktori
     */
    public MainApp() {

        palloData.add(punainen);
        palloData.add(vihrea);
        palloData.add(sininen);
        palloData.add(random);

    }

    /**
     * Palauttaa ObrservableList:n pallo olioista.
     * @return
     */
    public ObservableList<Pallo> getPersonData() {
        return palloData;
    }

    /**
     * Käynnistää primaryStagen. Asettaa ikkunan titlen ja iconin.
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("PalloMasin Pallomasiina");

        // Sovelluksen kuvake icon.
        this.primaryStage.getIcons().add(new Image("file:resources/images/mike-wazowski-icon.png"));

        initRootLayout();

        showPalloOverview();
    }

    /**
     * Alustaa ylapalkin(root layout) ja yrittaa avata edellisen istunnon.
     *
     */
    public void initRootLayout() {
        try {
            // Lataa ylapalkin(root layout) fxml tiedoston.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Avaa ylapalkin scenen (root layout).
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Antaa kontrollerille paaohjelman kaytto-oikeudet.
            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Koittaa ladata edeltävän session datan.
        File file = getPalloFilePath();
        if (file != null) {
            loadPalloDataFromFile(file);
        }
    }

    /**
     * nayttaa palloOverViewin Rootlayoutin sisällä.
     *
     */
    public void showPalloOverview() {
        try {
            // Lataa pallo overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/PalloOverview.fxml"));
            AnchorPane palloOverview = (AnchorPane) loader.load();

            // Asettele pallo overview root layoutin keskelle.
            rootLayout.setCenter(palloOverview);

            // Anna kontrollerille kaytto-oikeus paaohjelmaan.
            PalloOverviewController controller = loader.getController();
            controller.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Avaa edit-ikkunan valitulle pallolle. jos kayttaja painaa ok - uudet tiedot
     * tallennetaan ja suljetaan editointi ikkuna.
     *
     * @param pallo Editoitava pallo.
     * @return Palauttaa true jos kayttaja painoi ok.
     */
    public boolean showPalloEditDialog(Pallo pallo) {
        try {
            // Lataa PalloEditDialog fxml.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/PalloEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editoi Masia");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the person into the controller.
            PalloEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setPerson(pallo);

            // Set the dialog icon.
            dialogStage.getIcons().add(new Image("file:resources/images/Monster-inc-icon.png"));

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * Getteri pallo tiedoston asetuksille - "viimeisin istunto".
     * jos edellista istuntoa ei loydy palauttaa null.
     *
     * @return
     */
    public File getPalloFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        String filePath = prefs.get("filePath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    /**
     * Setteri ladatun istunnon tiedosto polulle.
     *
     * @param file File olio, joka voi olla myös null.
     */
    public void setPalloFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        if (file != null) {
            prefs.put("filePath", file.getPath());

            // paivita stagen title.
            primaryStage.setTitle("Pallomasi - " + file.getName());
        } else {
            prefs.remove("filePath");

            // paivita stagen title.
            primaryStage.setTitle("Pallomasi");
        }
    }

    /**
     * Lataa pallo datan tiedostosta obserablelistille ja korvaa auki olevan datan.
     *
     * @param file
     */
    public void loadPalloDataFromFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(PalloListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            // Lue XML data + unmarshal.
            PalloListWrapper wrapper = (PalloListWrapper) um.unmarshal(file);

            palloData.clear();
            palloData.addAll(wrapper.getPersons());

            // tallenna tiedostopolku rekisteriin.
            setPalloFilePath(file);

        } catch (Exception e) {
        	Alert alert = new Alert(AlertType.ERROR);
        	alert.setTitle("Error");
        	alert.setHeaderText("Dataa ei pystytty lataamaan");
        	alert.setContentText("Dataa ei pystytty lataamaan tiedostosta:\n" + file.getPath());

        	alert.showAndWait();
        }
    }

    /**
     * Tallenna auki oleva pallo-data tiedostoon.
     *
     * @param file
     */
    public void savePersonDataToFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(PalloListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrappaa pallo data.
            PalloListWrapper wrapper = new PalloListWrapper();
            wrapper.setPersons(palloData);

            // Marshallaus ja tallentaminen XML tiedostoon.
            m.marshal(wrapper, file);

            // Tallenna tiedosto polku rekisteriin.
            setPalloFilePath(file);
        } catch (Exception e) {
        	Alert alert = new Alert(AlertType.ERROR);
        	alert.setTitle("Error");
        	alert.setHeaderText("Dataa ei pystytty tallentamaan");
        	alert.setContentText("Dataa ei pystytty tallentamaan tiedostoon:\n" + file.getPath());

        	alert.showAndWait();
        }
    }

    /**
     * Palauttaa main-stagen.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Käynnistä data-yhteys ja sovellus.
     *
     * @param args
     */
    public static void main(String[] args) {

    	ct.start();
        launch(args);

    }
}