package RoboRobo;
import lejos.hardware.motor.MindsensorsGlideWheelMRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class Robotti {

	public static void main(String[] args) {
		RegulatedMotor leftMotor = new MindsensorsGlideWheelMRegulatedMotor(MotorPort.B);
		RegulatedMotor rightMotor = new MindsensorsGlideWheelMRegulatedMotor(MotorPort.C);
		RegulatedMotor[] sync = {leftMotor};

//		//eka suora, ja sen j?lkeen 90asteen k??nn?s oikealla moottorilla
		leftMotor.rotate(1150, true);
		rightMotor.rotate(1150, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		rightMotor.rotate(480, true);
		leftMotor.rotate(-480, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();

		//seuraava suora, k??nn?s ja pitk? suora
		leftMotor.rotate(1000, true);
		rightMotor.rotate(1000, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		leftMotor.rotate(730, true);
		rightMotor.rotate(-730, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		leftMotor.rotate(3150, true);
		rightMotor.rotate(3150, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();



		//kaarre
		rightMotor.synchronizeWith(sync);
		leftMotor.setSpeed(450);
		rightMotor.setSpeed(250);
		leftMotor.rotate(2050, true);
		rightMotor.rotate(1000, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		//puoliv�li
		rightMotor.synchronizeWith(sync);
		leftMotor.setSpeed(450);
		rightMotor.setSpeed(230);
		leftMotor.rotate(1510, true);
		rightMotor.rotate(590, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		leftMotor.setSpeed(450);
		rightMotor.setSpeed(450);
		
		leftMotor.waitComplete();
		rightMotor.waitComplete();	


		//kaarteen j?lkeinen suora
		leftMotor.rotate(1080, true);
		rightMotor.rotate(1080, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();	
		
		
		//K�ntyminen suoran j�lkeen
		rightMotor.rotate(-315, true);
		leftMotor.rotate(315, true);
		
		leftMotor.waitComplete();
		rightMotor.waitComplete();		
		
		
		//suoraan
		
		leftMotor.rotate(2100, true);
		rightMotor.rotate(2100, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();	
		
		//k��ntyy viimeiselle suoralle
		rightMotor.rotate(525, true);
		leftMotor.rotate(-525, true);
		
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		
		//takaisin kotiin suora
		
		leftMotor.rotate(2200, true);
		rightMotor.rotate(2200, true);
		leftMotor.waitComplete();
		rightMotor.waitComplete();	
		
		//k��nny

		rightMotor.rotate(960, true);
		leftMotor.rotate(-960, true);
		
		leftMotor.waitComplete();
		rightMotor.waitComplete();
		
		leftMotor.stop();
		rightMotor.stop();
		rightMotor.endSynchronization();



	}

}




