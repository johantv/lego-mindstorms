package pm.pallomasiina.kayttoliittyma.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import pm.pallomasiina.kayttoliittyma.MainApp;
import pm.pallomasiina.kayttoliittyma.view.PalloOverviewController;

/**
 * Saie joka pitaa huolen yhteydesta tietokoneen ja robotin valilla. Tama saie pitaa myos huolen
 * luettujen pallojen maaran paivittamisesta pallo-oliohin.
 * @author Ryhma 8
 *
 */
public class ConnectThread extends Thread {
    // Nämä tunnukset näkyvät saman pakkauksen luokkiin
	PalloOverviewController oc = new PalloOverviewController();
	int luku;
	static int sensorvalue;
	static int yhteisetAjokerrat = 0;

	//Kuvia
    Image vm = new Image(getClass().getResourceAsStream("/pics/2.png"));

	// Muuttujat yhteydelle
	Socket s;
	DataInputStream in;
	static DataOutputStream out;

	public static int painettu=2;


//--------------------------------------------------------------------------------

	// Lähetetään EV3:lle Integer-arvo, jolloin ohjelma käynnistyy
	/**
	 * Metodi joka kertoo robotille ett� nappia on painettu hy�dynt�en lukuarvoa.
	 * @return Palauttaa numeron 1, joka tarkoittaa ett� nappia on painettu.
	 * @throws IOException
	 */
	@FXML
	public static int paina() throws IOException {
		painettu=1;
		System.out.println(painettu);
		out.writeInt(painettu);
		out.flush();

		return painettu;
	}

	/**
	 * Yhdistet��n tietokone Robottiin
	 */
	public ConnectThread(){
		try{
			s = new Socket("10.0.1.1", 1111);
			in = new DataInputStream(s.getInputStream());
			out = new DataOutputStream(s.getOutputStream());
		}
		catch(Exception e) {
			System.out.println("ERROR IN SOCKET");
		}
	}
	/**
	 * Parametrillinen konstruktori s�ikeelle
	 * @param arvo
	 */
	public ConnectThread(int arvo){
		luku=arvo;
	}

	/**
	 * Getteri tietokoneelle l�hetetyn, robotin v�risensorin palauttamalle arvolle.
	 * @return palauttaa robotin v�risensorin palauttaman arvon
	 * @throws IOException
	 */
	public static int getSensorValue() throws IOException {
		return sensorvalue;
	}

	public int getSensorvalue() {
		return sensorvalue;
	}

	/**
	 * S�ileen suorittamat toiminnot.
	 * Lukee input streamist� int arvon ja saadun arvon perusteella lis�� lukua vastaavaan pallo-olion m��r��n +1.
	 */
	public void run() {
		try {
			while(true) {
				sensorvalue = in.readInt();
				System.out.println("printti" + sensorvalue);

		    	if(sensorvalue==0) {
		        	MainApp.punainen.setMaara(MainApp.punainen.getMaara()+1); // Lisätään punaisia palloja yhdellä
		        	System.out.println(MainApp.punainen.getMaara());
		    	}

		    	else if(sensorvalue==1){
		        	MainApp.vihrea.setMaara(MainApp.vihrea.getMaara()+1); // Lisätään vihreitä palloja yhdellä
		        	System.out.println(MainApp.vihrea.getMaara());
		    	}

		    	else if(sensorvalue==2){
		        	MainApp.sininen.setMaara(MainApp.sininen.getMaara()+1); // Lisätään sininen palloja yhdellä
		        	System.out.println(MainApp.sininen.getMaara());
		    	}

		    	else if(sensorvalue==3){
		        	MainApp.random.setMaara(MainApp.random.getMaara()+1);
		    	}

		    	else if(sensorvalue==4){
		    		// Tämä ei tee mitään, koska esinettä ei tunnistettu
		    	}
		    	else {
		        	MainApp.random.setMaara(MainApp.punainen.getMaara()+1);
		        	System.out.println(MainApp.punainen.getMaara());
		    	}

		    	System.out.println("sensorvalue update was SUCCESS");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}