package model;
/**
* Toimintojentila Kertoo onko jokin käyttäytyminen suoritettu hyödyntäen boolean muuttujia.
* "Toimii kytkimenä joka kertoo mikä operaatio pitäisi suorittaa"
*
* @author Ryhmä 8
* @version versionumero
*/
public class Toimintojentila {
	/**
	 * Toiminnon A tila.
	 */
	private static boolean actionA = true;
	/**
	 * Toiminnon B tila.
	 */
	private static boolean actionB = true;

//------------------------------------------------------------------------


/**
 * Setteri Toiminolle A
 * @param actionA Boolean muuttuja, joka määrittää tapahtuuko toiminto.
 */
	public static void setActionA(boolean actionA) {
		Toimintojentila.actionA = actionA;
	}

	/**
	 * Setteri toiminnolle B
	 * @param actionB Boolean muuttuja, joka määrittää tapahtuuko toiminto.
	 */
	public static void setActionB(boolean actionB) {
		Toimintojentila.actionB = actionB;
	}

	/**
	 * Getteri toiminolle A.
	 * @return Palauttaa toiminnan tilan.
	 */
	public static boolean getActionA() {
		return actionA;
	}
	/**
	 * Getteri toiminolle B.
	 * @return Palauttaa toiminnon tilan.
	 */
	public static boolean getActionB() {
		return actionB;
	}
}
