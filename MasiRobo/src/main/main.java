package main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import controller.AjaMoottoritAB;
import controller.StopMasi;
import lejos.robotics.subsumption.*;
import lejos.utility.Delay;
import model.MoottoritAB;
import model.Varianturi;
import controller.AjaMoottoriC;
import model.MoottoriC;
import model.Nappikomennot;
import model.SokettiRobotti;
import model.Toimintojentila;

public class main {

	public static void main(String[] args) {
		/**
		* Värianturi olio käyttäytymisille.
		*/
		Varianturi vari = new Varianturi();
		/**
		* SokettiRobotti olio, käyttäytymisille.
		*/
		SokettiRobotti s = new SokettiRobotti();
		/**
		* MoottoriAB olio, käyttäytymisille.
		*/
		MoottoritAB moottoritAB = new MoottoritAB();
		/**
		* MoottoriC olion luominen käyttäytymysiä varten.
		*/
		MoottoriC moottoriC = new MoottoriC();
		/**
		* Toimintojentila olion luonti käyttäytymisiä varten
		*/
		Toimintojentila tila = new Toimintojentila();


		/**
		* Käyttäytyminen Flipperien ohjaamiseen.
		*/
		Behavior valmisteleFlipperit = new AjaMoottoritAB(moottoritAB,vari,tila,s);
		/**
		* Käyttäytyminen pallojen syöttämiseen.
		*/
		Behavior laukaisePallo = new AjaMoottoriC(moottoriC, tila);
		/**
		*Käyttäytyminen hätäseis-ominaisuudelle.
		*/
		Behavior pysäytäKoodi = new Nappikomennot(moottoritAB);
		/**
		* Käyttäytyminen robotin toimintojen pysäyttämiseen
		*/
		Behavior stoppaaMasi = new StopMasi(moottoritAB);

		/**
		* Luodaan yhteys tietokonetta varten.
		*/
		s.kaynnistaYhteys();

		/**
		* Behavior lista joka sisältää robotin suorittamat käyttäytymiset.
		*/
		Behavior [] bArr = {laukaisePallo,pysäytäKoodi,stoppaaMasi,valmisteleFlipperit};

		/**
		* Arbitrator olio, joka hallitsee käyttäytymisiä. ottaa parametriksi käyttäytymis listan.
		*/
		Arbitrator arby = new Arbitrator(bArr);
		
		arby.go();
	}
}