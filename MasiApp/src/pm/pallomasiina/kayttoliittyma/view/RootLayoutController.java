package pm.pallomasiina.kayttoliittyma.view;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import pm.pallomasiina.kayttoliittyma.MainApp;
/**
* Kontrolleri ylapalkille(Root layoutille). Sisältää metodeja joita suoritetaan
* käyttäessä palkin ominaisuuksia.
*
*
* @author Ryhmä 8
* @version versionumero
*/
public class RootLayoutController {

    //Viittaus pääohjelmaan
    private MainApp mainApp;

    /**
     * Kutsutaan paaohjelmassa. Antaa paaohjelman kaytto-oikeuden RootLayotille.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Resettaa pallojen maaran luodessa uutta tiedostoa.
     */
    @FXML
    private void handleNew() {
    		mainApp.punainen.setMaara(0);
    		mainApp.vihrea.setMaara(0);
    		mainApp.sininen.setMaara(0);
    		mainApp.random.setMaara(0);
        mainApp.setPalloFilePath(null);
    }

    /**
     * Lataa-painikkeen metodi.
     * Avaa tiedostoikkunan, jonka kautta voi valita ladattavan pallojen listaus istunnon.
     */
    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        // Extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Avaa tallenna tiedostoikkuna
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {
            mainApp.loadPalloDataFromFile(file);
        }
    }

    /**
     * Tallenna-painikkeen metodi.
     * Tallentaa aukiolevan istunnon tiedot.
     *
     */
    @FXML
    private void handleSave() {
        File personFile = mainApp.getPalloFilePath();
        if (personFile != null) {
            mainApp.savePersonDataToFile(personFile);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Tallenna nimellä-painikkeen metodi.
     * Avaa tiedosto ikkunan, josta kayttaja valitee tiedoston mihin tallennetaan.
     */
    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        // Extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Nayta tallenna tiedostoikkuna
        File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());

        if (file != null) {
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            mainApp.savePersonDataToFile(file);
        }
    }

    /**
     * Tietoja-painikkeen metodi.
     * avaa alert ikkunan.
     */
    @FXML
    private void handleAbout() {
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setTitle("Pallomasi");
    	alert.setHeaderText("About");
    	alert.setContentText("Rolling balls since 1997!");

    	alert.showAndWait();
    }

    /**
     * Lopeta-napin metodi.
     * Sammuttaa ohjelman.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }

    /**
     * Tilastot-napin metodi.
     * Avaa alert ikkunan.
     */
    @FXML
    private void handleStatistics() {
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setTitle("Pallomasi");
    	alert.setHeaderText("Anteeksi!");
    	alert.setContentText("Tarvitsemme lisää palloja tilastoikkunan kehittämiseen.");

    	alert.showAndWait();

    }
}