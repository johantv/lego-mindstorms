package controller;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import model.MoottoritAB;
import model.SokettiRobotti;
import model.Toimintojentila;
import model.Varianturi;

public class AjaMoottoritAB implements Behavior {
    private Varianturi vari;
    private MoottoritAB flipperit;
    private volatile boolean suppressed = false;
    private boolean tehty;
    private Toimintojentila tila;
    private SokettiRobotti s;

    public AjaMoottoritAB (MoottoritAB flipperit, Varianturi vari, Toimintojentila tila, SokettiRobotti s) {
    	this.vari=vari;
    	this.flipperit=flipperit;
    	this.tila = tila;
    	this.s = s;

    	tehty = false;
    }

    public boolean takeControl() {
		return tila.getActionA();
	}

    //----Toiminto
	public void action() {
		suppressed = false;

		while(!suppressed) {
			flipperit.resetMoottorit();
			vari.scanVari();
			System.out.println(vari.getVari());

			// Värin perusteella ohjataan siivekkeet ja lähetetään data tietokoneelle
			if (vari.getVari()==0) {
				flipperit.ohjaaPunainen();
				s.lahetaDataaSoketti(0);
			}

			else if (vari.getVari()==1) {
				flipperit.ohjaaVihreä();
				s.lahetaDataaSoketti(1);
			}

			else if (vari.getVari()==2) {
				flipperit.ohjaaSininen();
				s.lahetaDataaSoketti(2);
			}

			else if (vari.getVari()==4) {
				// Tässä anturi ei tunnista mitään esinettä, myöskään siivekkeet eivät silloin liiku
				s.lahetaDataaSoketti(4);
			}

			else {
				flipperit.ohjaaMuu();
				s.lahetaDataaSoketti(3);
			}

			suppressed=true;

			tila.setActionA(false);
			tila.setActionB(true);
		}
	}

	public void suppress() {
		suppressed = true;
	}

    public boolean onkoTehty() {
    	if(tehty == true) {
    		tehty = false;
    		return false;
    	}

    	else {
    		return true;
    	}
    }
}