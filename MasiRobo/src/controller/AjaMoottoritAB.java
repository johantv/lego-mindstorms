package controller;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import model.MoottoritAB;
import model.SokettiRobotti;
import model.Toimintojentila;
import model.Varianturi;

/**
* Moottorien A&B(flipperit) käyttäytyminen. Tämä käyttäytyminen pitää huolen pallojen ohjaamisesta oikeisiin lokeroihin.
*
*
* @author Ryhmä 8
* @version versionumero
*/
public class AjaMoottoritAB implements Behavior {
    private Varianturi vari;
    private MoottoritAB flipperit;
    private volatile boolean suppressed = false;
    private Toimintojentila tila;
    private SokettiRobotti s;


//------------------------------------------------------------------------------
    /**
     * Konstruktori AjaMoottoritAB:lle
     * @param flipperit Konstruktorin käyttämä moottori.
     * @param vari	Konstruktorin käyttämä värisensori.
     * @param tila	Konstruktorin käyttämä toimintojentilan tarkistus.
     * @param s Konstruktorin käyttämä Soketti.
     */
    public AjaMoottoritAB (MoottoritAB flipperit, Varianturi vari, Toimintojentila tila, SokettiRobotti s) {
    	this.vari=vari;
    	this.flipperit=flipperit;
    	this.tila = tila;
    	this.s = s;


    }

    /**
     * Ehto käyttäytymisen käynnistämiseen.
     */
    public boolean takeControl() {
		return tila.getActionA();
	}

    /**
     * Suoritettava käyttäytyminen.
     * Nollaa moottorienasento-->Tarkista väri--->Säädä flipperien asento värisensorin getterin palauttaman arvon perusteella & ja lähetä tieto soketille.
     */
	public void action() {
		suppressed = false;

		while(!suppressed) {
			flipperit.resetMoottorit();
			vari.scanVari();
			System.out.println(vari.getVari());

			if (vari.getVari()==0) {
				flipperit.ohjaaPunainen();
				s.lahetaDataaSoketti(0);
			}

			else if (vari.getVari()==1) {
				flipperit.ohjaaVihreä();
				s.lahetaDataaSoketti(1);
			}

			else if (vari.getVari()==2) {
				flipperit.ohjaaSininen();
				s.lahetaDataaSoketti(2);
			}

			else if (vari.getVari()==4) {
				s.lahetaDataaSoketti(4);
			}

			else {
				flipperit.ohjaaMuu();
				s.lahetaDataaSoketti(3);
			}

			suppressed=true;

			tila.setActionA(false);
			tila.setActionB(true);
		}
	}

	/**
	 * Lopeta kayttäytyminen
	 */
	public void suppress() {
		suppressed = true;
	}

    }
