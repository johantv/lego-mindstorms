import lejos.robotics.SampleProvider;
import lejos.robotics.subsumption.*;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3IRSensor;

public class Etäisyyssensori implements Behavior {
	
	//muuttujat
	private EV3IRSensor distanceSensor;
	SampleProvider distanceProvider;
	static float[] sample;
	Port s4 = LocalEV3.get().getPort("S4");

	
	public Etäisyyssensori() {
		distanceSensor = new EV3IRSensor(s4);
	}
	
	
	private volatile boolean suppressed = false;
	
	public boolean takeControl() {
		return sample[0] > 20;
	}
	
	public void suppress() {
		suppressed = true;
	}
	
	public void action () {
		suppressed=false;
		
		//Kesken pitää kirjoittaa loppuun
		distanceProvider = distanceSensor.getDistanceMode();
		sample = new float [distanceProvider.sampleSize()];


		
		//tarkista etäisyys
		distanceProvider.fetchSample(sample,0);
	}


}
