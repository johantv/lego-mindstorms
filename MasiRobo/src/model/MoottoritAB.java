package model;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.motor.BasicMotor;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

// Moottorit siivekkeille (A ja B)
/**
* Moottorien A&B(flipperit) metodit ja muuttujat.
*
* @author Ryhmä 8
* @version versionumero
*/
public class MoottoritAB  {
	 /**
	  * Moottori-A.
	  */
    RegulatedMotor moottoriA = new EV3LargeRegulatedMotor(MotorPort.A);
    /**
     * Moottori-B.
     */
    RegulatedMotor moottoriB = new EV3LargeRegulatedMotor(MotorPort.B);


    /**
    * Astemäärä jolla A-moottori kääntyy oikealle.
    */
	private int Aoikealle = 35;
	/**
	* Astemäärä jolla A-moottori kääntyy vasemalle.
	*/
	private int Avasemmalle = -35;
	/**
	* Astemäärä jolla B-moottori kääntyy oikealle.
	*/
	private int Boikealle = -35;
	/**
	* Astemäärä jolla B-moottori kääntyy vasemmalle.
	*/
	private int Bvasemmalle = 35;


	/**
	* A-moottori asennon tarkistamiseen käytettävä muuttuja.(0=oletusasento)
	*/
	private int resetArvoA = 0;
	/**
	* B-moottorin asennon tarkistamiseen käytettävä muuttuja.(0=oletusasento).
	*/
	private int resetArvoB = 0;

    private boolean kesken = false;

//-------------------------------Ohjaaminen-----------------------------------------------
    /**
     * Ohjaa pallon punaisten pallojen lokeroon.
     * Päivittää flipperien asennot instanssimuuttujiin.
     */
    public void ohjaaPunainen() {

        moottoriA.setSpeed(500);


        moottoriB.rotate(Bvasemmalle);
        moottoriA.rotate(Avasemmalle);
        odotaMoottorit();
        Delay.msDelay(200);


        resetArvoA+=Avasemmalle;
        resetArvoB+=Bvasemmalle;
    }


    /**
     * Ohjaa pallon vihreiden pallojen lokeroon.
     * Päivittää flipperien asennot instanssimuuttujiin.
     */
    public void ohjaaVihreä() {
        moottoriA.setSpeed(500);


        moottoriB.rotate(Bvasemmalle);
        moottoriA.rotate(Aoikealle);
        odotaMoottorit();
        Delay.msDelay(200);


        resetArvoB+=Bvasemmalle;
        resetArvoA+=Aoikealle;
    }
    /**
     * Ohjaa pallon sinisten pallojen lokeroon.
     * Päivittää flipperien asennot instanssimuuttujiin.
     */
    public void ohjaaSininen() {
        moottoriA.setSpeed(500);


        moottoriB.rotate(Boikealle);
        moottoriA.rotate(Avasemmalle);
        odotaMoottorit();
        Delay.msDelay(200);


        resetArvoB+=Boikealle;
        resetArvoA+=Avasemmalle;

    }
    /**
     * Ohjaa pallon muun väristen pallojen lokeroon.
     * Päivittää flipperien sijainnit instanssi muuttujiin.
     */
    public void ohjaaMuu() {
        moottoriA.setSpeed(500);


        moottoriB.rotate(Boikealle);
        moottoriA.rotate(Aoikealle);
        odotaMoottorit();
        Delay.msDelay(200);


        resetArvoB+=Boikealle;
        resetArvoA+=Aoikealle;
    }

 //---------------------------------------------------------------------------

    /**
     * Odottaa että että mottorien liikkumminen on suoritettu loppuun.
     */
    public void odotaMoottorit() {
		moottoriB.waitComplete();
		moottoriA.waitComplete();
    }

    /**
     * Käännä moottoria A.
     * @param astetta Käännettävä astemäärä.
     */

    public void käännäA(int astetta) {
		moottoriA.rotate(astetta);

    }
    /**
     * Käännä moottoria B.
     * @param astetta Käännettävä aste määrä.
     */
    public void käännäB(int astetta) {
    		moottoriB.rotate(astetta);
    }


    /**
     * Asettaa moottorien asennon lähötilanteeseen(0-astetta).
     */
    public void resetMoottorit() {

    	try {
    		if (resetArvoA!=0 || resetArvoB !=0) {
    			käännäA(resetArvoA*-1);
    			käännäB(resetArvoB*-1);
    	        odotaMoottorit();
    		}
    	}

    	catch(Exception e) {
    		System.out.println("Moottoreiden resettaamisessa tapahtui virhe");
    	}

    	finally {

    		resetArvoA=0;
    		resetArvoB=0;
    	}
    }
 //---------------------------Getterit-------------------------------------
    /**
     * Getteri moottori A:n asennolle.
     * @return palauttaa astemäärään.
     */
    public int getMootorinAsentoA() {
    		return resetArvoA;
    }

    /**
     * Getteri moottori B:n asennolle.
     * @return	palauttaa astemäärään.
     */
    public int getMoottorinAsentoB() {
    		return resetArvoB;
    }

}