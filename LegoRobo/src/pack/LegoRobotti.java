package pack;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.MindsensorsGlideWheelMRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.Color;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class LegoRobotti {



	public static void main(String[] args) {
		EV3ColorSensor colorSensor;
		SampleProvider colorProvider;
		float[] colorSample;
		EV3IRSensor distanceSensor;
		SampleProvider distanceProvider;

		//Määritykset
		//moottoreiden määritys
		RegulatedMotor leftMotor = new MindsensorsGlideWheelMRegulatedMotor(MotorPort.B);
		RegulatedMotor rightMotor = new MindsensorsGlideWheelMRegulatedMotor(MotorPort.C);
		rightMotor.synchronizeWith(new RegulatedMotor[] {leftMotor});

		//etäisyyssensori
		Port s4 = LocalEV3.get().getPort("S4");
		distanceSensor = new EV3IRSensor(s4);
		distanceProvider = distanceSensor.getDistanceMode();
		float[] sample = new float[distanceProvider.sampleSize()];



		//värisensori
		Port s3 = LocalEV3.get().getPort("S3");
		SensorModes sensor = new EV3ColorSensor(s3);
		colorProvider = ((EV3ColorSensor)sensor).getRGBMode();
		colorSample = new float [colorProvider.sampleSize()];


		//Eka While eteenpäin
		distanceProvider.fetchSample(sample, 0);

		//Paina nappia
		System.out.println("Paina ENTER\n");
		Button.ENTER.waitForPressAndRelease();
		//System.out.println("\n\n\n\n\n\n");

		//luetaan värit

		colorProvider.fetchSample(colorSample, 0);
		int red = Math.round(colorSample[0]*765);
		int green = Math.round(colorSample[1]*765);
		int blue = Math.round(colorSample[2]*765);
		System.out.println("red: " + red + "\ngr: " + green + "\nbl:" + blue);
		System.out.println("\nVari 1 tallennettu.");
		Delay.msDelay(2000);
		System.out.println("\n\n\n\n\n\n\n\n");


		//tallennetaan eka väri
		int [] väri1 =  new int[3];
		väri1[0] = red;
		väri1[1] = green;
		väri1[2] = blue;

		//Delay.msDelay(2000);

		//Paina nappia
		System.out.println("Paina ENTER\n");
		Button.ENTER.waitForPressAndRelease();
		//Delay.msDelay(2000);
		colorProvider.fetchSample(colorSample, 0);

		//luetaan värit
		red = Math.round(colorSample[0]*765);
		green = Math.round(colorSample[1]*765);
		blue = Math.round(colorSample[2]*765);
		System.out.println("red: " + red + "\ngr: " + green + "\nbl:" + blue);
		System.out.println("\nVari 2 tallennettu.");
		Delay.msDelay(2000);

		System.out.println("\n\n\n\n\n\n\n\n");
		//tallennetaan toka väri
		int [] väri2 = new int[3];
		väri2[0] = red;
		väri2[1] = green;
		väri2[2] = blue;

		System.out.println("Laita paikoilleen ja paina ENTER");
		Button.ENTER.waitForPressAndRelease();
		System.out.println("\n\n\n\n\n\n\n\n");
		boolean looppi = true;
		while (looppi==true) {

			if (sample[0]>25){
				distanceProvider.fetchSample(sample, 0);
				rightMotor.startSynchronization();
				leftMotor.forward();
				rightMotor.forward();
				rightMotor.endSynchronization();
				Delay.msDelay(200);

			} else {
				rightMotor.startSynchronization();
				leftMotor.stop();
				rightMotor.stop();
				rightMotor.endSynchronization();
				looppi=false;
			}
		}
		distanceProvider.fetchSample(sample, 0);

		//While looppi pysähdys
		boolean looppi2=true;
		while (looppi2==true) {
			leftMotor.setSpeed(200);
			rightMotor.setSpeed(200);
			rightMotor.startSynchronization();
			leftMotor.backward();
			rightMotor.backward();
			rightMotor.endSynchronization();

			colorProvider.fetchSample(colorSample, 0);
			//Delay.msDelay(2000);


			//luetaan värejä
			red = Math.round(colorSample[0]*765);
			green = Math.round(colorSample[1]*765);
			blue = Math.round(colorSample[2]*765);


			//katsotaan kumman värin arvo on pienempi
			if (red > 130 && green <= 50 && blue <= 50) {
				rightMotor.startSynchronization();
				leftMotor.stop();
				rightMotor.stop();
				rightMotor.endSynchronization();

				System.out.println("Vari tunnistettu!");
				Delay.msDelay(1000);
				break;
			}




			//int currentDetectedColor = colorSensor.getColorID();
			//switch (currentDetectedColor) {
				//Kun väri on punainen
				//case :
				//pysaytaMoottorit(leftMotor, rightMotor);
					//break;

				//case Color.BLUE:
					//looppi2= false;
				//Kun väri on muu kuin case
				//default:
					//rightMotor.startSynchronization();
					//leftMotor.backward();
					//rightMotor.backward();
					//rightMotor.endSynchronization();
					//Delay.msDelay(2000);

					//break;

			}
			Delay.msDelay(250);
		}
}
