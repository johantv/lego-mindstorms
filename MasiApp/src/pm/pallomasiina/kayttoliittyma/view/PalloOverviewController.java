package pm.pallomasiina.kayttoliittyma.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import pm.pallomasiina.kayttoliittyma.MainApp;
import pm.pallomasiina.kayttoliittyma.model.ConnectThread;
import pm.pallomasiina.kayttoliittyma.model.Pallo;

import java.io.IOException;
import java.util.List;

/**
*Paaikkunan-ikkunan kontrolleri. Sisältää paaikkunan metodeja ja muuttujia.
*
* @author Ryhmä 8
* @version versionumero
*/
public class PalloOverviewController {
    @FXML
    private TableView<Pallo> personTable;
    @FXML
    private TableColumn<Pallo, String> palloVariColumn;
    @FXML
    private TableColumn<Pallo, Integer> palloMaaraColumn;

    @FXML
    private Label palloNameLabel;
    @FXML
    private Label palloCountLabel;


    @FXML
	private ImageView keskikuva;

    //Kuvia
    Image alotus = new Image(getClass().getResourceAsStream("/pics/10.png"));
    Image punainenMasi = new Image(getClass().getResourceAsStream("/pics/0.png"));
    Image vihreaMasi = new Image(getClass().getResourceAsStream("/pics/1.png"));
    Image sininenMasi = new Image(getClass().getResourceAsStream("/pics/2.png"));
    Image randomMasi = new Image(getClass().getResourceAsStream("/pics/3.png"));



    // Viittaus paasovellukseen.
    private MainApp mainApp;

    /**
     * Konstruktori
     *
     */
    public PalloOverviewController() {
    }

    /**
     * Alustaa Paaikkunan kontrollerin. Tama metodi kutsutaan automaattisesta,
     *  kun fxml tiedosto ladataan.
     */
    @FXML
    private void initialize() {
        // Alustaa "person table" kahdella kolumnilla.
        palloVariColumn.setCellValueFactory(cellData -> cellData.getValue().variProperty());
        palloMaaraColumn.setCellValueFactory(cellData -> cellData.getValue().maaraProperty().asObject());

    }

    /**
     *Kutsutaan paaohjelmassa. Antaa kontrollerille oikeudet kayttaa paaohjelmaa.
     *Lisaa myos observable listan pallo-oliot "person tableen"
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // lisaa observable list datan "person tableen"
        personTable.setItems(mainApp.getPersonData());
    }


    /**
     * kutsutaan kun kayttaja painaa Poista-paniketta.
     */
    @FXML
    private void handleDeletePerson() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            personTable.getItems().remove(selectedIndex);
        } else {
            // jos ei mitään valittuna.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ei Masia valittuna");
            alert.setHeaderText("Ei Masia valittuna");
            alert.setContentText("Valitse editoitava Masi taulukosta.");

            alert.showAndWait();
        }
    }
//------------------------SET KUVA----------------------------

    /**
     * Setteri paanayton keskikuvalle.
     * @param pic	Haluttu kuva Image muodossa.
     */
    public void setPic(Image pic) {
    		keskikuva.setImage(pic);
    }

//------------------------Käynnistä---------------------------

    /**
     * Kutsutaan Käynnistä-painiketta painettaessa. Kutsuu ConnectThreadin
     * paina-metodia, joka kertoo robotille, etta se voi aloittaa toimintansa.
     * @throws IOException
     */
    @FXML
    	public void kaynnista() throws IOException {
    		ConnectThread.paina();
    	}

    /**
     * Kutsutaan Kuva-painiketta painaessa.
     * Metodi joka tarkistaa robotin lukeman varin ja vaihtaa keskikuvan
     * vastaamaan varia, jonka sensori luki.
     */
    @FXML
    public void checkPic() {
    		try {
				if(ConnectThread.getSensorValue()==0) {
		    			keskikuva.setImage(punainenMasi);

				}

				else if(ConnectThread.getSensorValue()==1) {
		    			keskikuva.setImage(vihreaMasi);

				}

				else if(ConnectThread.getSensorValue()==2) {
		    			keskikuva.setImage(sininenMasi);

				}

				else if(ConnectThread.getSensorValue()==3) {
		    			keskikuva.setImage(randomMasi);

				}

			} catch (IOException e) {

				e.printStackTrace();
			}
    }




    //-----------------------------------------------------------------
    /**
     * Kutsutaan kun kayttaja painaa Lisaa-painiketta.
     * avaa editointinti ikkunan.
     *
     */
    @FXML
    private void handleNewPerson() {


        Pallo tempPerson = new Pallo();
        boolean okClicked = mainApp.showPalloEditDialog(tempPerson);
        if (okClicked) {
            mainApp.getPersonData().add(tempPerson);
        }
    }

    /**
     * Kutsutaan kun kayttaja painaa Editoi-painiketta.
     * avaa editointi ikkunan valitun pallon tiedoilla, jos pallo oli valittuna.
     *
     */
    @FXML
    private void handleEditPerson() {
        Pallo valittuPallo = personTable.getSelectionModel().getSelectedItem();
        if (valittuPallo != null) {
            boolean okClicked = mainApp.showPalloEditDialog(valittuPallo);
            if (okClicked) {
                //showPersonDetails(valittuPallo);
            }

        } else {
            // Jos ei mitään valittuna.
            Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ei Masia valittuna");
            alert.setHeaderText("Ei Masia valittuna");
            alert.setContentText("Valitse editoitava Masi taulukosta.");

            alert.showAndWait();
        }
    }
}