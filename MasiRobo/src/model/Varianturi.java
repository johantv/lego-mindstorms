package model;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;
/**
* Värianturin tarkoitus on kertoa väri muille käyttäytymisille. Listaa värejä ja palauttaa niiden arvon kokonaislukuna 0-4.
*
* @author Ryhmä 8
* @version versionumero
*/
public class Varianturi {
	/**
	* Portti EV3-konsolille.
	*/
	Port port1 = LocalEV3.get().getPort("S1");
	/**
	* Värisensori.
	*/
    SensorModes sensor2 = new EV3ColorSensor(port1);
	/**
	* Sample Provideri.
	*/
    SampleProvider colorProvider = ((EV3ColorSensor)sensor2).getRGBMode();
	/**
	* Lista värien tallentamiselle.
	*/
    float[] sample2 = new float[colorProvider.sampleSize()];

    /**
     * Lukee värin värisensorilla ja tallentaa sen listaan.
     */


 //--------------------------------------------------------------------------------------
    public void scanVari() {
    	// Värien tallettaminen listaan
    	colorProvider.fetchSample(sample2, 0);
        sample2[0] = Math.round(sample2[0]*765);
        sample2[1] = Math.round(sample2[1]*765);
        sample2[2] = Math.round(sample2[2]*765);

        // Väriarvojen tulostaminen EV3:n näytölle
        System.out.println("p: "+ sample2[0] +" v: "+ sample2[1]+ " s: " + sample2[2]);
    }

    /**
     * Getteri, joka kertoo minkä värin sensori luki.
     * @return	Palauttaa luetun värin arvona 0-4 (Punainen=0, Vihreä=1, Sininen=2, Muu=3, Tyhjä=4).
     */
    public int getVari() {
    	if(sample2[0]>=10 && sample2[1]<9 && sample2[2] <5) {
    		return 0;   													//punainen
    	}

    	else if (sample2[0]<7 && sample2[1]>=10 && sample2[2]<7) {
    		if (sample2[0]==5 && sample2[1]==11 && sample2[2]==5) {
    			return 4;													//ei esinettä
    		}
    		else if(sample2[0]==5 && sample2[1]==10 && sample2[2]==5) {
    			return 4;													//ei esinettä
    		}
    		else if(sample2[0]==6 && sample2[1]==10 && sample2[2]==6) {
    			return 4;													//ei esinettä
    		}
    		else {
    			return 1;													//vihreä
    		}
    	}

    	else if (sample2[0]<11 && sample2[1]>=8 && sample2[2]>=10) {
    		return 2;														//sininen
    	}

    	else {
    		return 3;														//muu
    	}
    }
}