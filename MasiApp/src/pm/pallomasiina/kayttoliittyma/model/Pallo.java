package pm.pallomasiina.kayttoliittyma.model;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import pm.pallomasiina.kayttoliittyma.view.PalloOverviewController;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
* Pallo-olio luokka. Sisältää konstruktorit, getterit ja setterit palloille.
*
*
* @author Ryhmä 8
* @version versionumero
*/
public class Pallo {

	private Image kuva;
    private final StringProperty vari;
    private final IntegerProperty maara;


    /**
     * parametriton Konstruktori pallo-oliolle
     */
    public Pallo() {
        this(null, 0);
    }

    // Konstruktori parametreillä
    /**
     * Parametrillinen konstruktori pallo-oliolle.
     * @param vari	Pallon vari.
     * @param maara	Pallon maara.
     */
    public Pallo(String vari, int maara) {
        this.vari = new SimpleStringProperty(vari);
        this.maara = new SimpleIntegerProperty(maara);
    }

    /**
     *Getteri varille
     * @return
     */
    public String getVari() {
        return vari.get();
    }
    /**
     * Setteri varille
     * @param vari
     */
    public void setVari(String vari) {
        this.vari.set(vari);
    }

    /**
     * Getteri varin Propertylle.
     * @return palauttaa varin.
     */
    public StringProperty variProperty() {
        return vari;
    }

    /**
     * Getteri Määrälle
     * @return
     */
    public int getMaara() {
        return maara.get();
    }

    /**
     * Setteri maaralle
     * @param maara Pallo-olion maara.
     */
    public void setMaara(int maara) {
        this.maara.set(maara);
    }

    /**
     * Getteri maaran propertylle
     * @return	Palauttaa maaran.
     */
    public IntegerProperty maaraProperty() {
        return maara;
    }
}