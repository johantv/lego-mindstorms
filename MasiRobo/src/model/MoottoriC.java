package model;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.motor.BasicMotor;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;
/**
* Moottori C:n(nostimen) metodit ja muuttujat.
*
* @author Ryhmä 8
* @version versionumero
*/
public class MoottoriC  {
	/**
	* Moottori-C
	*/
    RegulatedMotor moottoriC = new EV3MediumRegulatedMotor(MotorPort.C);


    /**
     * Moottorin nopeus astetaan 500 astetta/s.
     * Moottori pyörähtää 540 astetta liikuttaen nostinta yhden kierroksen.
     * 1000ms viive pallojen liikumiselle.
     */
    public void suoraan() {
        moottoriC.setSpeed(500);
        moottoriC.rotate(540);
        Delay.msDelay(1000);
    }

    /**
     * Pysäytä moottori.
     */
    public void seis() {
    	moottoriC.stop();
    }


    }
