import java.util.Random;
import lejos.hardware.lcd.LCD;

import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.*;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.*;
import lejos.robotics.Color;
import lejos.utility.Delay;
public class SiivoojaRobo {

	//Muuttujat
	DifferentialPilot pilotti;
	Random randomi;
	int ajaNopeasti = 12;
	int ajaHitaasti = 2;

	//Värit
	static int red;
	static int green;
	static int blue;


	//Moottorit & robo
	RegulatedMotor moottoriB = new EV3LargeRegulatedMotor(MotorPort.B);
	RegulatedMotor moottoriC = new EV3LargeRegulatedMotor(MotorPort.C);

	final double HALKAISIJA = 4.32;
	final double RAIDELEVEYS = 15;

	//VäriSensori
	EV3ColorSensor colorSensor;
	SampleProvider colorProvider;
	float[] colorSample;

	//Distance
	EV3IRSensor distanceSensor;
	SampleProvider distanceProvider;
	static float[] sample;



	//Pyöri randomisti
	public SiivoojaRobo() {

		//int stop = ;

		//Etäisyys määritykset
		Port s4 = LocalEV3.get().getPort("S4");
		distanceSensor = new EV3IRSensor(s4);
		distanceProvider = distanceSensor.getDistanceMode();
		sample = new float [distanceProvider.sampleSize()];
		moottoriC.synchronizeWith(new RegulatedMotor[] {moottoriB});



		//Colorsensorin määritykset
		Port s3 = LocalEV3.get().getPort("S3");
		SensorModes sensor = new EV3ColorSensor(s3);
		colorProvider = ((EV3ColorSensor)sensor).getRGBMode();
		colorSample = new float [colorProvider.sampleSize()];

		//värin tallennus
		colorProvider.fetchSample(colorSample,0);
		red = Math.round(colorSample[0]*765);
		green = Math.round(colorSample[1]*765);
		blue = Math.round(colorSample[2]*765);
		float[] colorSample2 = new float [colorProvider.sampleSize()];

		//System.out.println("red: " + red + "\ngreen: " + green + "\nblue: " + blue);
		//Delay.msDelay(2000);
		//Pilotti
		pilotti = new DifferentialPilot (HALKAISIJA, RAIDELEVEYS, moottoriB , moottoriC);
		pilotti.setLinearSpeed(ajaNopeasti);
		randomi = new Random();

		//ota alkuetäisyys
		distanceProvider.fetchSample(sample,0);

		//Käynnistä robo
		System.out.println("Paina nappia");
		Button.ENTER.waitForPressAndRelease();
		System.out.println("\n\n\n\n\n\n\n\n");
		Sound.beep();

		while (true) {

			int red2, green2, blue2;

			colorProvider.fetchSample(colorSample2,0);
			red2 = Math.round(colorSample2[0]*765);
			green2 = Math.round(colorSample2[1]*765);
			blue2 = Math.round(colorSample2[2]*765);
			//System.out.println("red: " + red2 + "\ngreen: " + green2 + "\nblue: " + blue2);
			//Tarkista etäisyys alussa
			distanceProvider.fetchSample(sample,0);

			moottoriB.setSpeed(300);
			moottoriC.setSpeed(300);
			moottoriC.startSynchronization();
			moottoriB.forward();
			moottoriC.forward();
			moottoriC.endSynchronization();



			//System.out.println("red: " + red2 + "\ngreen: " + green2 + "\nblue: " + blue2);
			//Tarkista etäisyys alussa
			distanceProvider.fetchSample(sample,0);

			if (sample[0] <= 15 || red2 >= 100 && green2 <= 50 && blue2 <= 50) {
				System.out.println("Este havaittu.");
				System.out.println("\n\n\n\n\n\n\n\n");

				moottoriC.startSynchronization();
				moottoriB.stop(true);
				moottoriC.stop(true);
				moottoriC.endSynchronization();
				moottoriB.rotate(-360, true);
				moottoriC.rotate(-360, true);
				moottoriB.waitComplete();
				moottoriC.waitComplete();
				pilotti.rotate(Math.random()*360);
			}
			if (distanceSensor.getRemoteCommand(3)==1 || distanceSensor.getRemoteCommand(3)==2 || distanceSensor.getRemoteCommand(3)==3
					||distanceSensor.getRemoteCommand(3)==4) {
				moottoriB.stop(true);
				moottoriC.stop(true);
				System.out.println("Ohjelma seis, nappia painettu.");
				Delay.msDelay(2000);
				break;
			}
		}
	}

	//Ja käyntiin
	public static void main(String[] args) {
		new SiivoojaRobo();
	}
}


