package model;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
/**
* Soketti jolla muodostetaan yhteys tietokoneeseen.
*
* @author Ryhmä 8
* @version versionumero
*/
public class SokettiRobotti {
	ServerSocket serv;
	Socket s;
	DataOutputStream out;
	static DataInputStream in;


	/**
	 * Metodi yhteyden muodostamiseen.
	 */
	public void kaynnistaYhteys() {
		try {
			serv = new ServerSocket(1111);
			Socket s = serv.accept(); //Odotetaan yhteyden muodostamista
			out = new DataOutputStream(s.getOutputStream());	 // Datan lähetys robolta PC:lle
			in = new DataInputStream(s.getInputStream());		// Datan vastaanotto PC:ltä robolle
			in.readInt(); // Luetaan vastaanotettava integer-arvo
			System.out.println("Yhteys on käynnistetty");
		}
		catch(Exception e) {
			System.out.println("Yhteyden muodostus Error");
		}
	}

	/**
	 * Metodi datan lähettämiseen.
	 * @param data lähetettävä data.
	 */
	public void lahetaDataaSoketti(int data) {
		try {
			out.writeInt(data); // Lähetetään tietokoneelle Integer-arvo
			out.flush(); // MUISTA flush aina kirjoittamisen jälkeen
		}
		catch(Exception e) {
			System.out.println("Datan lähetys Error");
		}
	}
	/**
	 * Getteri DataInputStreamille.
	 * @return palauttaa datainputStreamiin lähetetyn arvon.
	 * @throws IOException
	 */
	public int getDataInputVirta() throws IOException {
		int avustaja=in.readInt();
		return avustaja;
	}
}
